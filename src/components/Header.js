import React from 'react';

import config from './../../config.js';

export default function Footer() {
  return (
    <header id="header">
      <h1>{config.heading}</h1>
      <p>{config.subHeading}</p>
    </header>
  );
}
