module.exports = {
  siteTitle: 'Midas Touch Technologies', // <title>
  manifestName: 'Midas Touch',
  manifestShortName: 'Midas Touch', // max 12 characters
  manifestStartUrl: '/',
  manifestBackgroundColor: '#663399',
  manifestThemeColor: '#663399',
  manifestDisplay: 'standalone',
  manifestIcon: 'src/assets/img/logo.png',
  pathPrefix: `/`, // This path is subpath of your hosting https://domain/portfolio
  // social
  heading: 'Midas Touch',
  subHeading:
    `Midas Touch is a registered tech solutions entity that's currently operates in the Eastern Cape Region, providing much needed modern age tech services.
     We offer branding, web design, system design & development, digital marketing and software engineering.`,
  socialLinks: [
    {
      icon: 'fa-facebook',
      name: 'Facebook',
      url: 'https://facebook.com/theanubhav',
    },
    {
      icon: 'fa-envelope-o',
      name: 'Email',
      url: 'mailto:ayabongaqwabi@gmail.com',
    },
  ],
};
